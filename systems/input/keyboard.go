package input

import (
	"context"
	"log"
	"sync"
	"time"

	"gitlab.com/repub/guv"
)

// IOEvent for a keyboard thingy
type IOEvent struct {
	Name string
}

// Type for the guv.Event interface
func (IOEvent) Type() string { return "IOEvent" }

// Update for the guv.Event interface
func (IOEvent) Update() {}

// InputEntity holds a key and associated action
type InputEntity struct {
	guv.BaseEntity
	*KeyComponent
	*ActionComponent
}

type KeyComponent struct {
	Key string
}

// ActionComponent takes action on something during updates
// kind of like a callback
type ActionComponent struct {
	Action func(interface{})
}

// InputSystem doesn't do much yet
type InputSystem struct {
	lastCalled time.Time
	lastEvents []IOEvent
	ctx        context.Context

	entities []*InputEntity
	term     *Term
	lock     *sync.Mutex
}

func New(ctx context.Context, wg *sync.WaitGroup) *InputSystem {
	return &InputSystem{
		ctx:        context.Background(),
		lastCalled: time.Now(),
		entities:   make([]*InputEntity, 0),
		term:       NewTerm(wg),
		lock:       &sync.Mutex{},
		lastEvents: make([]IOEvent, 0),
	}
}

// Add an InputEntity to the system to be monitored.  these
// are callback entities; which comobine a Key with an action.
// they are not unique on a key; so you can have multiple actions
// tied to a single key press.
func (is *InputSystem) Add(be guv.BaseEntity, kc *KeyComponent, ac *ActionComponent) {
	is.entities = append(is.entities, &InputEntity{be, kc, ac})
}

// Update checks to see if the last events is one that we
// by default nothing is handled.
func (is *InputSystem) Update(time.Duration) error {
	is.lock.Lock()
	events := is.lastEvents
	is.lastEvents = make([]IOEvent, 0)
	is.lock.Unlock()

	for _, event := range events {
		for _, e := range is.entities {
			if KeyNames[e.KeyComponent.Key] == event.Name {
				e.ActionComponent.Action(event)
			}
		}
	}
	return nil
}

// Remove will remove an input
// TODO test - make sure the removal works correctly for multiple keys
func (is *InputSystem) Remove(b guv.BaseEntity) {
	for i := len(is.entities) - 1; i >= 0; i-- {
		e := is.entities[i]
		if e.ID() == b.ID() {
			is.entities = append(is.entities[:i-1], is.entities[i-1:]...)
		}
	}
}

// WatchKeys is a blocking func to get key and interupt events
// this persists the last event to the InputSystem, and also dispatches
// it as an event to the Guv to trigger an update run.
// Some sort of debounce, sync, or queue may be needed later:
// I believe that droppping anything but the Update component
// from this makes sense as the ActionComponent could handle
// making more specific calls; such as converting
//   KeyArrowRight -> NextPage
//   KeyArrowLeft  -> LastPage
// to handle this idiomatically based on what screen we are in
func (is *InputSystem) WatchKeys(ctx context.Context, eventChan chan<- guv.Event) {
	is.term.wg.Add(1)
	go func() {
		// close out the reading of the file to
		// make sure we cleanly exit when the context
		// is closed.
		<-ctx.Done()
		is.term.Close()
	}()
	defer close(eventChan)
	buf := make([]byte, 16)
	for {
		log.Println("Loop")
		if ctx.Err() != nil {
			return
		}
		n, err := is.term.in.Read(buf)

		if err != nil {
			return
		}
		if n > 0 {
			strKey := string(buf[:n])
			if name, ok := KeyNames[strKey]; ok {
				event := IOEvent{Name: name}
				is.lock.Lock()
				is.lastEvents = append(is.lastEvents, event)
				is.lock.Unlock()
				eventChan <- event
				// fmt.Printf("name %s\n\r", name)
			}
		} else {
			<-time.After(20 * time.Millisecond)
		}
	}
}

func (is *InputSystem) Name() string { return "InputSystem" }
