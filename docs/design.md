# Notes on design

These are note from a widget and windowing system from a game i'm working on.  Its predomanently ECS based with message passing.

The notes come from engo/engine; and I'd like to streamline the message protocol and callback system for a general UI system.

### Design pillars

1) Children don't change parents.  Eg: padding and margines are done by their own entities.  This means layout and flow goes one way and is gready

eg:

```
+--ContainerAbsolute----------+
|                             |
|   +---MarginContainer----+  |
|   |  10x10               |  |
|   | +-Label------------+ |  |
|   | |  Hi              | |  |
|   | +------------------+ |  |
|   |                      |  |
|   +----------------------+  |
|                             |
+-----------------------------+
```

Its easier to see as pseudocode.  For example; ContainerAbsolute lets you set offset, and layers everything from first to last.
Other conainers can have other rules.  Like a top down list; tileing etc.

```
progressbar:
 ContainerAbsolute:
  Layers:
   - Background:
       Rectangle:
         Color: grey
         MaxWidth: 64
         MaxHeight: 8
         Offset: 0,0
   - Bar:
       Rectangle:
         Color: blue
         MaxWidth: 64
         MaxHeight: 8
         Offset: 0,0
         Width: `<entityID:percentage>`
   - Border:
       Rectangle:
         Color: Black
         MaxWidth: 64
         MaxHeight: 8
         Weight: 2
         Offset: 0,0
```


Example widget tree; making use of above, psuedocode

```
modal:
  VerticalListContainer:
    - textBox:
         MaxWidth: 256
         MaxHeight: 80
         TextContent: "You are this far along the way to Enlightment, guv"
    - progressbar:
         MaxWidth: 256
         MaxHeight: 8
         Width: `<entityID:enlightment_progress>`
    - Button:
        Label:
          Text: "No Enlightment for me!"
```

Decorations add as you want.

1b) figure out templating system so you can easily define something like `progressbar` from
primatives and reuse it.

A Widget is either something drawn, or a container of other widgets.

2) Try not to become a turing machine like css.  We can't control how callbacks
function: so that means straight up we can't prevent that from being turing
complete. cause it is.  but we can prevent the layout engine from getting
turing complete.

example in yaml that sucks; but could start as a cheap standin for a GML language
```yaml
progressBar: &progressBar
  ContainerAbsolute:
    Layers:
      Background:
        Rectangle:
          Color: Grey
        MaxWidth: 64
        MaxHeight: 8
        Offset:
          X: 0
          Y: 0
      Fill:
        Rectangle:
          Color: Blue
        MaxWidth: 64
        MaxHeight: 8
        Offset:
          X: 0
          Y: 0
      Border:
        Box:
          Color: Blue
          Weight: 2
        MaxWidth: 64
        MaxHeight: 8
        Offset:
          X: 0
          Y: 0


healthBar: 
  <<: *progressBar
  Overrides:
    ContainerAbsolute:
      Fill:
        Rectangle:
          Color: Red

```

## Messages

Reveiw QT's slot mechanisms; but also learn from fuchia and other modern
microkernals: signal systems and interupts can be really bad.

Engo takes a 'mailbox' approach: but its more like a post board
