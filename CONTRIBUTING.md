# Contributing Guidelines

## Did you find a bug

1. Check if its reported
2. Open an issue in the gitlab issues.  Ensure it has reproduction steps

## Do you want a feature

1. Check if its already in the backlog.
2. Describe the feature in an issue.  ensure the value or goal is clearly defined.  If you have ideas on how to implement it make sure those are described in a block separate from the value.

## Do you want to contribute code

1. Make sure you have an issue/feature you're working against; submit a MR referencing it.  Tests == good.
2. Ensure your stuff actually runs.

## Documentation bad?

1. Make an MR.
