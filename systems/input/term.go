package input

import (
	"log"
	"os"
	"sync"
	"syscall"

	"golang.org/x/term"
)

const (
	ioctlReadTermios  = 0x5401 // syscall.TCGETS
	ioctlWriteTermios = 0x5402 // syscall.TCSETS
)

// NewTerm returns a Terminal with read only.
// TODO transition to SIGIO or EPOLL when we get to the optimization
// stages for battery (normal poling bad for batteries)
func NewTerm(wg *sync.WaitGroup) *Term {
	in, err := os.OpenFile("/dev/tty", os.O_RDONLY, 0)
	if err != nil {
		panic(err)
	}
	t := Term{
		in: in,
		wg: wg,
	}
	state, err := term.MakeRaw(1)
	t.savedState = state
	if err != nil {
		return nil
	}

	return &t

}

// Term encapsulates the reading from the raw tty for events
type Term struct {
	in         *os.File
	termios    syscall.Termios
	wg         *sync.WaitGroup
	savedState *term.State
}

// Close off al the resources and restore the terminal state.
// then end waiting
func (t *Term) Close() {
	log.Println("Closing it down")
	term.Restore(1, t.savedState)
	t.in.Close()
	t.wg.Done()

}
