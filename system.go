package guv

import "time"

// System processes entities on an update cycle.  Its up the implementer to make a method of adding members to the
// system.  Removal is done by ID from BasicEntity
// `Update` is the means of processing the entities.  the `dt time.Duration` is the time since the last update was called
type System interface {
	Update(dt time.Duration) error
	Remove(e BaseEntity)
	Name() string
}
