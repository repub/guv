package guv_test

import (
	"context"
	"testing"
	"time"

	"gitlab.com/repub/guv"
)

type TestEvent struct {
	value interface{}
}

type UpdateTrigger struct {
	value interface{}
}

func (te TestEvent) Type() string {
	return "TestEvent"
}

func (ut UpdateTrigger) Update() {}
func (ut UpdateTrigger) Type() string {
	return "UpdateTrigger"

}

type TestSystem struct {
	called time.Duration
}

func (ts *TestSystem) Update(dt time.Duration) error {
	ts.called = dt
	return nil
}

func (ts *TestSystem) Remove(guv.BaseEntity) {}
func (ts *TestSystem) Name() string          { return "TS" }

func TestGuvEventHandler(t *testing.T) {
	g := guv.New(context.Background())
	go g.EventLoop()

	okChan := make(chan struct{})
	eventIn := make(chan guv.Event)
	g.AddEventChan(eventIn)

	g.AddEventHandler("TestEvent", func(e guv.Event) {
		okChan <- struct{}{}
	})
	eventIn <- &TestEvent{}
	select {
	case <-time.After(time.Second):
		t.Log("Took too long for event update")
		t.Fail()
	case _, ok := <-okChan:
		if !ok {
			t.Log("okChan was closed before we could read it")
			t.Fail()
		}
	}
	close(eventIn)
	close(okChan)
}

func TestGuvEventUpdaterHandler(t *testing.T) {
	g := guv.New(context.Background())
	go g.EventLoop()
	ts := &TestSystem{}
	g.AddSystem(ts)

	okChan := make(chan struct{})
	eventIn := make(chan guv.Event)
	g.AddEventChan(eventIn)

	g.AddEventHandler("UpdateTrigger", func(e guv.Event) {
		okChan <- struct{}{}
	})
	eventIn <- UpdateTrigger{}
	select {
	case <-time.After(time.Second):
		t.Log("Took too long for event update")
		t.Fail()
	case _, ok := <-okChan:
		if !ok {
			t.Log("okChan was closed before we could read it")
			t.Fail()
		}
	}
	close(eventIn)
	close(okChan)
}
