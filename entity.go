package guv

import "sync/atomic"

var currentID uint64

// BaseEntity just has an ID.  May change in the future
// if the ID is 0 you didn't create it right
type BaseEntity struct {
	id uint64
}

// ID is locked up due to needing the setting to be consistent and thread safe
//
func (be BaseEntity) ID() uint64 {
	return be.id
}

// NewBaseEntity returns a basic entity with a unique ID (int64 limits apply)
func NewBaseEntity() BaseEntity {
	return BaseEntity{
		id: atomic.AddUint64(&currentID, 1),
	}
}
