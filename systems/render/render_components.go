package render

import (
	"image"
	"image/color"
	"image/draw"
)

// SolidFill is a render.Drawer that outputs a solid color. Good for backgrounds.
type SolidFill struct {
	Color color.Color
}

// Draw a solid color to the image
func (sf SolidFill) Draw(img draw.Image) {
	draw.Draw(img, img.Bounds(), image.NewUniform(sf.Color), image.Point{}, draw.Src)
}
